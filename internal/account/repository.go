package account

import (
	"context"

	"gitlab.com/patipol.thamdee/banking/internal/entity"
)

// Repository encapsulates the logic to access albums from the data source.
type Repository interface {
	// Get returns the album with the specified album ID.
	Get(ctx context.Context, id string) (entity.Album, error)
	// Count returns the number of albums.
	Count(ctx context.Context) (int, error)
	// Query returns the list of albums with the given offset and limit.
	Query(ctx context.Context, offset, limit int) ([]entity.Album, error)
	// Create saves a new album in the storage.
	Create(ctx context.Context, album entity.Album) error
	// Update updates the album with given ID in the storage.
	Update(ctx context.Context, album entity.Album) error
	// Delete removes the album with given ID from the storage.
	Delete(ctx context.Context, id string) error
}

// repository persists albums in database
type repository struct {
}

// NewRepository creates a new album repository
func NewRepository() Repository {
	return repository{}
}

// Get reads the album with the specified ID from the database.
func (r repository) Get(ctx context.Context, id string) (entity.Album, error) {
	album := entity.Album{}
	return album, nil
}

// Create saves a new album record in the database.
// It returns the ID of the newly inserted album record.
func (r repository) Create(ctx context.Context, album entity.Album) error {
	return nil
}

// Update saves the changes to an album in the database.
func (r repository) Update(ctx context.Context, album entity.Album) error {
	return nil
}

// Delete deletes an album with the specified ID from the database.
func (r repository) Delete(ctx context.Context, id string) error {
	return nil
}

// Count returns the number of the album records in the database.
func (r repository) Count(ctx context.Context) (int, error) {
	return 1, nil
}

// Query retrieves the album records with the specified offset and limit from the database.
func (r repository) Query(ctx context.Context, offset, limit int) ([]entity.Album, error) {
	albums := []entity.Album{}
	return albums, nil
}
