package account

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/patipol.thamdee/banking/internal/errors"
	"gitlab.com/patipol.thamdee/banking/pkg/pagination"
)

func RegisterHandlers(e *echo.Echo, service Service) {
	res := resource{service}

	e.GET("/albums/<id>", res.get)
	e.GET("/albums", res.query)
	// e.GET("/", func(c echo.Context) error {
	// 	return c.String(http.StatusOK, "Hello, World!")
	// })
	// the following endpoints require a valid JWT
	e.POST("/albums", res.create)
	e.PUT("/albums/<id>", res.update)
	e.DELETE("/albums/<id>", res.delete)
}

type resource struct {
	service Service
}

func (r resource) get(c echo.Context) error {
	album, err := r.service.Get(c.Request().Context(), c.Param("id"))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, album)
}

func (r resource) query(c echo.Context) error {
	ctx := c.Request().Context()
	count, err := r.service.Count(ctx)
	if err != nil {
		return err
	}
	pages := pagination.NewFromRequest(c.Request(), count)
	albums, err := r.service.Query(ctx, pages.Offset(), pages.Limit())
	if err != nil {
		return err
	}
	pages.Items = albums
	return c.JSON(http.StatusOK, pages)
}

func (r resource) create(c echo.Context) error {
	var input CreateAlbumRequest
	if err := c.Bind(&input); err != nil {
		// r.logger.With(c.Request.Context()).Info(err)
		return errors.BadRequest("")
	}
	album, err := r.service.Create(c.Request().Context(), input)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusCreated, album)
}

func (r resource) update(c echo.Context) error {
	var input UpdateAlbumRequest
	if err := c.Bind(&input); err != nil {
		// r.logger.With(c.Request.Context()).Info(err)
		return errors.BadRequest("")
	}

	album, err := r.service.Update(c.Request().Context(), c.Param("id"), input)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, album)
}

func (r resource) delete(c echo.Context) error {
	album, err := r.service.Delete(c.Request().Context(), c.Param("id"))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, album)
}
