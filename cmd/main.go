package main

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/patipol.thamdee/banking/internal/account"
)

func main() {
	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})
	setHandlers(e)
	e.Logger.Fatal(e.Start(":1323"))
}

func setHandlers(e *echo.Echo) {
	account.RegisterHandlers(e, account.NewService(account.NewRepository()))
}
