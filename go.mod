module gitlab.com/patipol.thamdee/banking

go 1.15

require (
	github.com/go-ozzo/ozzo-validation/v4 v4.3.0
	github.com/google/uuid v1.1.2
	github.com/labstack/echo/v4 v4.1.17
	gitlab.com/patipol.thamdee/utils v0.0.0-20201219111100-71fda5cfac98 // indirect
)
